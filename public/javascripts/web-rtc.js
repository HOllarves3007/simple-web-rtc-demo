$(document).ready(function(){
    var webrtc = new SimpleWebRTC({
        // the id/element dom element that will hold "our" video
        localVideoEl: 'localVideo',
        // the id/element dom element that will hold remote videos
        remoteVideosEl: 'remoteVideos',
        // immediately ask for camera access
        autoRequestMedia: true
    });

// we have to wait until it's ready
    webrtc.on('readyToCall', function () {
        // you can name it anything
        webrtc.joinRoom('some_random_room_name_22567');
    });

    webrtc.connection.on('message', function(data){
        if(data.type == 'chat') {
            console.log('chat recieved', data);
            $('#messages').append('<br>' + data.payload.nick + ':  ' + data.payload.message);
        }
    });

    $('.chatContainer').on('click', '#send', function(){
        var msg = $('#text').val();
        var name = $('#name').val();
        webrtc.sendToAll('chat', {message: msg, nick: name});
        $('#messages').append('<br> You: ' + msg);
        $('#text').val('');
    });

    $('.chatContainer').on('click', '#clear', function(){
        $('#messages').empty();
    });
});

